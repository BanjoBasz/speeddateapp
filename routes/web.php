<?php


Route::get('/', 'VragenController@randomVraag');
Route::get('/index', 'VragenController@index');
Route::get('/vragen/{id}', 'VragenController@show');
