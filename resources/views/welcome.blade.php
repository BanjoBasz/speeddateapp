<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>SpeeddateApp</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
              background: #f4c4f3; /* fallback for old browsers */
              background: -webkit-linear-gradient(left, #f4c4f3, #fc67fa);
              background: -o-linear-gradient(left, #f4c4f3, #fc67fa);
              background: linear-gradient(to right, #f4c4f3, #fc67fa); /* Chrome 10-25, Safari 5.1-6 */ /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
              font-family: 'Roboto', sans-serif;
                      }
                        h1{
                          color:white;
                          text-align:center;
                          margin-top:300px;
                          font-size:2em;
                        }
        </style>
    </head>
    <body>
        <h1>{{$vraag}}</h1>
    </body>
</html>
