<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Vraag;
use View;
class VragenController extends Controller
{
    public function index(){
      return Vraag::all();
    }

    public function show($id){
      return Vraag::find($id);
    }

    public function randomVraag(){
      $count =  count(Vraag::all())-1;
      $random = rand(1,$count);
      $vraag = Vraag::find($random);
      return View::make('welcome', array('vraag' => $vraag->vraag));
    }
}
