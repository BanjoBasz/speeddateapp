<?php

use Illuminate\Database\Seeder;

class VragenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vragen = ['Wat zou jij doen met Jar Jar Binks als je hem tegen zou komen op straat?',
'Hoe heet je?',
'Heb je een geheime fetisj?',
'Wat staat bovenaan je Bucket list',
'Wat zijn jouw hobbies',
'Ananas op pizza?',
'Wat is je lievelingseten',
'Als je een tatoeage moest nemen, waarover neem je hem dan? ',
'Als heel de wereld tegen je lijkt te staan. Naar wie ga je toe?',
'Wat is jouw favoriete dierentuin?',
'Waar ligt je passie? ',
'Can I be the Wouter to your Loise?',
'Windows of Apple?',
'Wat zijn je interesses?',
'Wat is je favoriete programmeertaal?',
'Star wars of star trek?',
'Wat zou je doen met een miljoen euro?',
'Wat is je Favoriete film(?',
'Wat zijn je hobbys?',
'Heb je alle star wars films gezien',
'beschrijf onze romantische toekomst samen',
'Is dit je eerste date?',
'Wat is je naam',
'Android of IOS',
'Meest gebruikte app',
'favorite programmeertaal',
'Do you know da weh',
'Waar hou je van?',
'Ben je een sporter',
'Wat is/zijn je favorieten muziek genre (s)',
'Speed of a swallow',
'Wat doe je als een beer bergafwaarts achter je aan rent?',
'Wat voor muziek luister je naar?',
'Heb je ook een hekel aan mensen? ',
'Wat vind jij van Star Wars?',
'Waar kom je vandaan?',
'Vind jij tuinkabouters ook zo leuk?',
'Wat voor eten houd je van',
'Wat zou je doen als je moest kiezen tussen netflix of amazon prime?',
'sudo rm / -R ?',
'Other than money, what would make your life a little bit better today?',
'Verwacht Jeroen ook van jou om sfw te zijn?',
'Ben je een metalhead?',];

        foreach($vragen as $vraag){
          $this->insertIntoDb($vraag);
        }
    }

    public function insertIntoDb($vraag){
      DB::table('vragen')->insert([
       'vraag' => $vraag,
     ]);
    }
}
